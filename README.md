# python 爬虫多线程

## requests

### headers

- `User-Agent`,模拟浏览器
- `Range`,下载资源范围

```python
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/' +
    '537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.66',
    "Range": "bytes=%s-%s" % (self.startpos, self.endpos)
}
```

### 请求:

```python
    #timeout,参数分别是请求超时,读取超时
    res = requests.get(self.url, headers=headers,timeout=(60,60))
```

### 读取文件大小

```python
    size = res.headers['Content-Length']
```

## log

### 创建日志文件

```python
def createLogTxt(logDir):
    '''
    根据现在的时间生成日志文件
    logDir:日志文件目录
    return:logFile 返回log文件路径(目录加文件名)
    '''
    logTxtName = time.strftime("%Y-%m-%d %H_%M_%S", time.localtime())
    logFile = logDir+logTxtName+'.txt'
    with open(logFile, "a") as f:
        f.write('log初始化\n')
    return logFile
```

### 日志记录

```python
def log(logFile, logInfo):
    '''
    记录日志
    logFiel 日志文件路径
    '''
    timeStr = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    # 以日期来创建log文件
    with open(logFile, "a") as f:
        f.write(timeStr+'-----'+logInfo)
```

## fd 对象

### fileno() 方法返回一个整型的文件描述符(file descriptor FD 整型)，可用于底层操作系统的 I/O 操作。

```python
    fileno = f.fileno()
```

### os.dup() 方法用于复制文件描述符 fd。

```python
    dup = os.dup(fileno)  # 复制文件句柄
```

### os.fdopen() 方法用于通过文件描述符 fd 创建一个文件对象，并返回这个文件对象。

```python
    fd = os.fdopen(dup, 'rb+', -1)  # -1使用系统自定义缓存
```

## 线程

```python
threading.BoundedSemaphore(threadNum) # 信号量
mtdl = MulThreadDownload(url, start, end, fd)
mtdl.start()
mtd_list.append(mtdl)


#线程等待
for t in mtd_list:
    t.join()
```
